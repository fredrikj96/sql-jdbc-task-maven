package com.example.java_sql_jdbc_task_maven.data;


import com.example.java_sql_jdbc_task_maven.dto.SpenderDto;
import com.example.java_sql_jdbc_task_maven.models.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Service
public class CustomerRepository {

    @Value("${spring.datasource.url}")
    private String url;
    private Connection con = null;

    public List<Customer> getCustomers() {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<Customer> getCustomerById(int id) {

        List<Customer> customers = new ArrayList<>();
        try { con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement("SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer.customer_id = ?");
             ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<Customer> getCustomerByName(String fName) {

        List<Customer> customers = new ArrayList<>();
        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer.first_name= ?");
            ps.setString(1, fName);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }
    public List<Customer> getCustomersOffset(int limit, int offset) {

        List<Customer> customers = new ArrayList<>();
        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer LIMIT  ? OFFSET  ?");
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    public List<Customer> insertCustomer(Customer customer) {

        List<Customer> customers = new ArrayList<>();
        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, customer.getCustomer_id());
            ps.setString(2, customer.getFirst_name());
            ps.setString(3, customer.getLast_name());
            ps.setString(4, customer.getCountry());
            ps.setString(5, customer.getPostal_code());
            ps.setString(6, customer.getPhone());
            ps.setString(7, customer.getEmail());

            int result = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<Customer> updateCustomerName(String a, String b) {

        List<Customer> customers = new ArrayList<>();
        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("UPDATE customer SET first_name = ? WHERE first_name = ?");
            ps.setString(1, a);
            ps.setString(2, b);

            int result = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    public Map<Integer, String> getCountries() {

        Map<Integer, String> customers = new LinkedHashMap<>();
        try { con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement("SELECT country, COUNT(*) FROM customer GROUP BY (customer.country) ORDER BY COUNT DESC");
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.put(rs.getInt("COUNT"),
                            rs.getString("country"));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Map<Integer, SpenderDto> getHighestSpenders() {

        Map<Integer, SpenderDto> customers = new LinkedHashMap<>();
        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer.customer_id, first_name, last_name, SUM (total) FROM customer INNER JOIN invoice ON invoice.customer_id = customer.customer_id GROUP BY customer.customer_id ORDER BY SUM DESC");
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.put(rs.getInt("customer_id"),
                          new SpenderDto(rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getDouble("SUM")));

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }



}
