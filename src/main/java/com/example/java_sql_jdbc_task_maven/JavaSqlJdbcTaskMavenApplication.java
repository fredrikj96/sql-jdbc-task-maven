package com.example.java_sql_jdbc_task_maven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSqlJdbcTaskMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSqlJdbcTaskMavenApplication.class, args);
    }

}
