package com.example.java_sql_jdbc_task_maven.controllers;

import com.example.java_sql_jdbc_task_maven.data.CustomerRepository;
import com.example.java_sql_jdbc_task_maven.dto.SpenderDto;
import com.example.java_sql_jdbc_task_maven.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/customers")
public class CustomerController {

    private CustomerRepository customerRepository;


    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping()
    public List<Customer> getAllCustomers() {
        return customerRepository.getCustomers();
    }

    @GetMapping("/id/{id}")
    public List<Customer> getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/name/{fName}")
    public List<Customer> getCustomerByName(@PathVariable String fName) {
        return customerRepository.getCustomerByName(fName);
    }

    @GetMapping("/{limit}/{offset}/")
    public List<Customer> getCustomersOffset(@PathVariable int limit, @PathVariable int offset) {
        return customerRepository.getCustomersOffset(limit, offset);
    }

    @PostMapping("/insert")
    public List<Customer> insertCustomer(@RequestBody Customer insertCustomer) {
        return customerRepository.insertCustomer(insertCustomer);
    }

    @PatchMapping("/{a}/{b}")
    public List<Customer> updateCustomerName(@PathVariable String a, @PathVariable String b) {
        return customerRepository.updateCustomerName(a, b);
    }

    @GetMapping("/country")
    public Map<Integer, String> getCountries() {return customerRepository.getCountries();}

    @GetMapping("/highestspenders")
    public Map<Integer, SpenderDto> getHighestSpenders() {return customerRepository.getHighestSpenders();}
}
