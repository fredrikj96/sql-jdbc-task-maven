package com.example.java_sql_jdbc_task_maven.dto;

public class SpenderDto {
    private String first_name;
    private String last_name;
    private double sum;

    public SpenderDto(String first_name, String last_name, double sum) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.sum = sum;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
